package com.wyc.blog;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @ClassName NotFoundException
 * @Author 王韫琛
 * @Date 2021/4/9 9:00
 * @Version 1.0
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
/**
 * @ResponseStatus(HttpStatus.NOT_FOUND)
 *  会将NotFoundException作为资源找不到的状态
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

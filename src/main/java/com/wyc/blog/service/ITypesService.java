package com.wyc.blog.service;

import com.wyc.blog.po.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ITypesService {

    /**
     * 新增
     * @param type
     * @return
     */
    Type saveType(Type type);

    /**
     * 查询
     * @param id
     * @return
     */
    Type getType(Long id);

    /**
     * 分页查询
     * @param pageable
     * @return
     */
    Page<Type> listType(Pageable pageable);

    /**
     * 修改
     * @param id
     * @param type
     * @return
     */
    Type updateType(Long id,Type type);

    /**
     * 删除
     * @param id
     */
    void deleteType(Long id);

    /**
     * 根据名称查询
     * @param name
     * @return
     */
    Type getTypeByName(String name);

    /**
     * 返回所有的数据
     * @return
     */
    List<Type> listType();

    List<Type> listTypeTop(Integer size);
}

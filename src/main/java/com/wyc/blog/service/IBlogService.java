package com.wyc.blog.service;

import com.wyc.blog.po.Blog;
import com.wyc.blog.vo.BlogQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IBlogService {

    /**
     * 查询blog
     * @param id
     * @return
     */
    Blog getBlog(Long id);

    /**
     * 查询一组数据
     * @return
     */
    Page<Blog> listBlog(Pageable pageable, BlogQuery blog);

    /**
     * 新增一个blog
     * @param blog
     * @return
     */
    Blog saveBlog(Blog blog);

    /**
     * 修改一个blog
     * @return
     */
    Blog updateBlog(Long id,Blog blog);

    /**
     * 删除一个blog
     * @param id
     */
    void deleteBlog(Long id);

    /**
     * 分页数据
     * @param pageable
     * @return
     */
    Page<Blog> listBlog(Pageable pageable);

    /**
     * 推荐博客列表
     * @param size
     * @return
     */
    List<Blog> listRecommendBlogTop(Integer size);
}

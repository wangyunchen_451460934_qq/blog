package com.wyc.blog.service;

import com.wyc.blog.po.User;

public interface IUserService {

    /**
     * 根据用户名查询密码
     * @param username 用户名
     * @param password 密码
     * @return
     */
    User checkUser(String username,String password);
}

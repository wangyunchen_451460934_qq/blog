package com.wyc.blog.service.impl;

import com.wyc.blog.dao.UserRepository;
import com.wyc.blog.po.User;
import com.wyc.blog.service.IUserService;
import com.wyc.blog.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @ClassName IUserServiceImpl
 * @Author 王韫琛
 * @Date 2021/4/24 1:04
 * @Version 1.0
 */
@Service
public class IUserServiceImpl implements IUserService {

    @Autowired
    private UserRepository userRepository;

    /**
     * 根据用户名查询密码
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @Override
    public User checkUser(String username, String password) {
        User user = userRepository.findByUsernameAndPassword(username, MD5Util.code(password));
        return user;
    }
}

package com.wyc.blog.web;

import com.wyc.blog.po.Tag;
import com.wyc.blog.service.IBlogService;
import com.wyc.blog.service.ITagService;
import com.wyc.blog.service.ITypesService;
import com.wyc.blog.vo.BlogQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * @ClassName IndexController
 * @Author 王韫琛
 * @Date 2021/4/8 23:49
 * @Version 1.0
 */
@Controller
public class IndexController {

    @Autowired
    private IBlogService blogService;

    @Autowired
    private ITypesService typesService;
    @Autowired
    private ITagService tagService;
    @RequestMapping("/")
    public String index(@PageableDefault(size = 8, sort = {"updateTime"}, direction = Sort.Direction.DESC) Pageable pageable,
                        Model model){
        model.addAttribute("page",blogService.listBlog(pageable));
        model.addAttribute("types",typesService.listTypeTop(5));
        model.addAttribute("tags", tagService.listTagTop(5));
        model.addAttribute("recommendBlogs", blogService.listRecommendBlogTop(8));
        return "index";
    }
    @RequestMapping("/blog")
    public String blog(){
        return "blog";
    }
}

package com.wyc.blog.dao;

import com.wyc.blog.po.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @ClassName UserRepository
 * @Author 王韫琛
 * @Date 2021/4/24 16:24
 * @Version 1.0
 */
public interface UserRepository extends JpaRepository<User,Long> {

    /**
     * 根据用户名和密码查询数据库的用户信息
     * @param username
     * @param password
     * @return
     */
    User findByUsernameAndPassword(String username,String password);
}
